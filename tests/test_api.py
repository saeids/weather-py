import sys
import os
# Append the parent directory of this file to the sys.path
# This allows us to import the api module from the project root directory
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from api import get_weather

def test_get_weather_returns_none_for_invalid_city():
    """
    This test function checks the behavior of the get_weather function when provided with an invalid city name.
    It asserts that the get_weather function returns None in this case, as expected.

    No parameters are required, as the test function internally calls get_weather with a hard-coded invalid city name.

    Returns:
    This function does not return a value. Instead, it uses an assertion to signal a test failure to pytest if get_weather 
    does not behave as expected.
    """
    assert get_weather('invalidcity') is None
