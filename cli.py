import argparse
from api import get_weather

def main():
    """
    Main function to run the CLI application.
    """
    # Create the top-level parser.
    parser = argparse.ArgumentParser(description='Get the current weather for a given city.')
    parser.add_argument('city', help='The name of the city to get the weather for.')
    args = parser.parse_args()

    