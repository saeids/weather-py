import requests

# Added actual API key from OpenWeatherMap.
API_KEY = 'd35d95e5c9e485597377a9ba5ef88e3b'

def get_weather(city_name):
    """
    Fetch weather data for a given city from the OpenWeatherMap API.

    Parameters:
    city_name (str): The name of the city to fetch weather data for.

    Returns:
    dict: A dictionary containing the weather data if the API request is successful, None otherwise.
    """
    # Base URL of the OpenWeatherMap API.
    base_url = 'http://api.openweathermap.org/data/2.5/weather'
    
    # Parameters for the API request.
    params = {
        'q': city_name,  # The city name.
        'appid': API_KEY  # Your API key.
    }
    
    # Send a GET request to the API.
    response = requests.get(base_url, params=params)

    # If the request is successful (status code is 200), return the data as a Python dictionary.
    if response.status_code == 200:
        return response.json()
    # If the request is not successful, return None.
    else:
        return None
