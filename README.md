# WeatherPy
Welcome to WeatherPy! This project is designed to provide real-time weather data from cities around the world. It's a Python-based application that consists of a Command-Line Interface (CLI) and a RESTful API, both used to fetch and present data from the OpenWeatherMap API in a user-friendly way.

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites
- Python 3.6 or later
- Docker (for containerization)
- An API key from OpenWeatherMap (You can get one from https://openweathermap.org/)

### Installation
1. Clone this repository to your local machine.

    ```bash
    git clone https://gitlab.com/saeids/weather-py.git
    ```

2. Install necessary libraries.
    ```bash
    pip install requests
    pip install flask
    ```

3. Set your OpenWeatherMap API key as an environment variable named `WEATHER_API_KEY`.

    ```bash
    export WEATHER_API_KEY=d35d95e5c9e485597377a9ba5ef88e3b
    ```
**Note:** Storing API keys and other sensitive information directly in code is generally not a best practice. It's included here for simplicity.

### Files:
- `cli.py`: This file contains the logic for your Command-Line Interface (CLI).
- `api.py`: This file contains the logic to communicate with the OpenWeatherMap API.

## Running the App
To run the app, navigate to the project directory and use the Python command followed by the city name:

```bash
python cli.py <city-name>
```

## Running the Tests
Tests are performed using pytest. To run the tests, navigate to the project directory and execute the following command:

```bash
pytest
```

## API Documentation
The OpenWeatherMap API is used to fetch the weather data. The base URL for the API is `http://api.openweathermap.org/data/2.5/weather`. You can get the weather of a city by appending `?q={city name}&appid={API key}` to the base URL. 

## Features
- Fetch real-time weather data for any city
- Display key weather information: temperature, humidity, wind speed, and general conditions
- RESTful API endpoint for fetching weather data
- Dockerized application for consistent performance across platforms
- Comprehensive error handling for seamless user experience

## Built With
- Python
- Docker
- OpenWeatherMap API (for weather data)

## Contributing
This section will be updated as the project progresses.

## License
This section will be updated as the project progresses.

## Acknowledgements
This project is part of a school assignment in studying DevOps and developing Python applications.

